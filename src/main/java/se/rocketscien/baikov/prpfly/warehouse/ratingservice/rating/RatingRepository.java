package se.rocketscien.baikov.prpfly.warehouse.ratingservice.rating;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RatingRepository extends CrudRepository<RatingEntity, Long> {

    Optional<RatingEntity> findByProductId(long productId);

}
