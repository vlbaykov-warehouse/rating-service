package se.rocketscien.baikov.prpfly.warehouse.ratingservice.rating;

import org.mapstruct.Mapper;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.RatingDto;

@Mapper
public interface RatingMapper {

    RatingDto entityToDto(RatingEntity entity);

}
