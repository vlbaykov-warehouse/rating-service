package se.rocketscien.baikov.prpfly.warehouse.ratingservice.rating;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;

import javax.validation.constraints.Min;

@RestController
@RequestMapping("/api/v1/rating")
@Api("Rating API")
@RequiredArgsConstructor
@Validated
public class RatingController {

    private final RatingService ratingService;

    @PostMapping(path = "/product/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Request rating by product ID")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Rating not found"),
    })
    public ResponseDto<?> requestRatingByProductId(
            @PathVariable @ApiParam(name = "Product ID", required = true) @Min(value = 0, message = "product-id.invalid") long productId
    ) {
        ratingService.requestRatingByProductId(productId);
        return ResponseDto.empty();
    }

}
