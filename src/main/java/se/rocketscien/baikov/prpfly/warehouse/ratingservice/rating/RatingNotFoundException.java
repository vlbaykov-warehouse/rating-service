package se.rocketscien.baikov.prpfly.warehouse.ratingservice.rating;

import org.springframework.http.HttpStatus;
import se.rocketscien.baikov.prpfly.warehouse.common.exception.ValidationException;

public class RatingNotFoundException extends ValidationException {

    public RatingNotFoundException(long productId) {
        super(HttpStatus.NOT_FOUND, "rating.not-found", productId);
    }

}
