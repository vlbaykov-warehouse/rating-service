package se.rocketscien.baikov.prpfly.warehouse.ratingservice;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@ConfigurationProperties("rating-service")
@Data
@Validated
public class RatingServiceProperties {

    @Valid
    private Kafka kafka = new Kafka();

    @Data
    public static class Kafka {

        @Valid
        private KafkaTopic topic = new KafkaTopic();

    }

    @Data
    public static class KafkaTopic {

        @NotBlank
        @Pattern(regexp = "^[\\w.-]+$")
        String name;

        @Min(1)
        int partitions;

        @Min(1)
        int replicas;

    }

}
