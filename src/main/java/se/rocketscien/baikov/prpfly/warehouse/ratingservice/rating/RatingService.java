package se.rocketscien.baikov.prpfly.warehouse.ratingservice.rating;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.RatingDto;
import se.rocketscien.baikov.prpfly.warehouse.ratingservice.RatingServiceProperties;

@Service
@RequiredArgsConstructor
@Slf4j
public class RatingService {

    private final RatingServiceProperties properties;
    private final RatingRepository ratingRepository;
    private final RatingMapper ratingMapper;
    private final KafkaTemplate<Long, RatingDto> kafkaTemplate;

    public void requestRatingByProductId(long productId) {
        RatingEntity entity = ratingRepository.findByProductId(productId)
                .orElseThrow(() -> new RatingNotFoundException(productId));
        sendRatingToKafkaTopic(entity);
    }

    private void sendRatingToKafkaTopic(RatingEntity entity) {
        String topicName = properties.getKafka().getTopic().getName();
        RatingDto ratingDto = ratingMapper.entityToDto(entity);
        log.info("Sending rating [{}] to topic [{}]", ratingDto, topicName);
        kafkaTemplate.send(topicName, entity.getProductId(), ratingDto);
    }

}
