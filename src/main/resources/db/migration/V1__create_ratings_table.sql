CREATE TABLE ratings
(
    id         BIGSERIAL PRIMARY KEY,
    version    BIGINT        NOT NULL,
    product_id BIGINT        NOT NULL,
    rating     NUMERIC(5, 2) NOT NULL
);

CREATE INDEX ratings_product_idx ON ratings (product_id);