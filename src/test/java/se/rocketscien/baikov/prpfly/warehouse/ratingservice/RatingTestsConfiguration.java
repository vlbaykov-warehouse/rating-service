package se.rocketscien.baikov.prpfly.warehouse.ratingservice;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto;

import java.util.List;
import java.util.Map;

@TestConfiguration
@Slf4j
public class RatingTestsConfiguration {

    @Bean
    public KafkaContainer kafka() {
        log.info("Starting kafka container");
        KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka"));
        kafka.start();
        log.info("Kafka container started");
        return kafka;
    }

    @Bean
    @DependsOn("kafka")
    public NewTopic ratingTopic(RatingServiceProperties properties) {
        RatingServiceProperties.KafkaTopic topic = properties.getKafka().getTopic();
        log.info("Creating kafka topic [{}]", topic);
        return TopicBuilder.name(topic.getName())
                .partitions(topic.getPartitions())
                .replicas(topic.getReplicas())
                .build();
    }

    @Bean
    public ProducerFactory<?, ?> producerFactory(KafkaContainer kafka) {
        return new DefaultKafkaProducerFactory<>(
                Map.of(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafka.getBootstrapServers()),
                new LongSerializer(),
                new JsonSerializer<>()
        );
    }

    @Bean
    public KafkaConsumer<Long, PriceDto> kafkaConsumer(RatingServiceProperties properties, KafkaContainer kafka) {
        Map<String, Object> config = Map.of(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafka.getBootstrapServers()
        );
        JsonDeserializer<PriceDto> jsonDeserializer = new JsonDeserializer<>();
        jsonDeserializer.trustedPackages("*");
        KafkaConsumer<Long, PriceDto> consumer = new KafkaConsumer<>(
                config,
                new LongDeserializer(),
                jsonDeserializer
        );
        consumer.assign(List.of(new TopicPartition(properties.getKafka().getTopic().getName(), 0)));
        return consumer;
    }

}
