package se.rocketscien.baikov.prpfly.warehouse.ratingservice;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.testcontainers.junit.jupiter.Testcontainers;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.RatingDto;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(RatingTestsConfiguration.class)
@EnableConfigurationProperties(RatingServiceProperties.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Testcontainers
class RatingTests {

    static String basicUrl = "http://localhost:{port}/api/v1/rating/";
    static String requestRatingByProductIdUrl = basicUrl + "product/{productId}";
    static ParameterizedTypeReference<ResponseDto<?>> EMPTY_RESPONSE_TR = new ParameterizedTypeReference<>() {
    };
    static RatingDto rating1 = RatingDto.builder()
            .productId(1L)
            .rating(BigDecimal.ONE.setScale(2, RoundingMode.HALF_UP))
            .build();

    @LocalServerPort
    int port;
    @Autowired
    RatingServiceProperties properties;
    @Autowired
    TestRestTemplate restTemplate;
    @Autowired
    KafkaConsumer<Long, PriceDto> kafkaConsumer;

    @Test
    @DisplayName("Request rating by product id - success")
    void testSuccessfulRequestRatingByProductId() {
        ResponseDto<?> response = requestRatingByProductId(rating1.getProductId());
        assertNotNull(response);
        assertNull(response.getResult());
        assertNull(response.getError());
        ConsumerRecords<?, ?> records = kafkaConsumer.poll(Duration.ofSeconds(15));
        assertNotNull(records);
        assertEquals(1, records.count());
        ConsumerRecord<?, ?> record = records.iterator().next();
        assertNotNull(record);
        assertEquals(rating1.getProductId(), record.key());
        assertEquals(rating1, record.value());
    }

    @ParameterizedTest
    @MethodSource("getFailedRequestRatingByProductIdArguments")
    @DisplayName("Request rating by product id - fail")
    void testFailedRequestRatingByProductId(
            Object productId,
            ResponseDto.ResponseErrorDto expectedError
    ) {
        ResponseDto<?> response = requestRatingByProductId(productId);
        assertNotNull(response);
        assertNull(response.getResult());
        assertEquals(expectedError, response.getError());
    }

    private Stream<Arguments> getFailedRequestRatingByProductIdArguments() {
        return Stream.of(
                Arguments.of(
                        999,
                        error(HttpStatus.NOT_FOUND, "Rating for product with id [999] not found")
                ),
                Arguments.of(
                        "text",
                        error(HttpStatus.BAD_REQUEST, "Failed to convert " +
                                "value of type 'java.lang.String' to required type 'long'; nested exception is " +
                                "java.lang.NumberFormatException: For input string: \"text\"")
                ),
                Arguments.of(
                        -50,
                        error(HttpStatus.BAD_REQUEST, "Invalid product id [-50]")
                )
        );
    }

    private ResponseDto<?> requestRatingByProductId(Object productId) {
        return restTemplate
                .exchange(requestRatingByProductIdUrl, HttpMethod.POST, HttpEntity.EMPTY, EMPTY_RESPONSE_TR, port, productId)
                .getBody();
    }

    private static ResponseDto.ResponseErrorDto error(HttpStatus status, String message) {
        return new ResponseDto.ResponseErrorDto(status.value(), message);
    }

}
